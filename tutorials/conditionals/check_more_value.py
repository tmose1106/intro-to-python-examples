user_value = int(input("Please enter a number: "))

if user_value > 0:
    print("Your value is greater than zero.")
elif user_value < 0:
    print("Your value is less than zero.")
else:
    print("Your value is exactly zero.")
