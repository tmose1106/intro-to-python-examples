""" Functions dealing with integers. """

def get_maximum_integer(int_list):
    """ Find the integer with the greatest value out of a list of integers. """
    current_max = None

    for an_int in int_list:
        #  We can not compare a number to none, so if it is None, set it to
        # an integer we can use
        if current_max == None:
            current_max = an_int
        elif an_int > current_max:
            current_max = an_int

    return current_max
