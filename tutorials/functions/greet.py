def greet_someone(name, greeting="Hello", repeat=1):
    # Create a formatted greeting string using two of the arguments
    greeting_string = "{}, {}!".format(greeting, name)
    # For each repeat... (we don't need the "temporary" variable)
    for _ in range(repeat):
        # Print a the formatted string
        print(greeting_string)

print("Trial #1")
greet_someone("World")
print("\nTrial #2")
greet_someone("Cruel World", greeting="Goodbye")
print("\nTrial #3")
greet_someone("Cruel World", "Goodbye")
print("\nTrial #4")
greet_someone("Alice", repeat=3, greeting="Greetings")
