def print_shopping_list(*items, **details):
    """ Print a formatted shopping list. """
    # Print a header for details
    print("Details:\n--------")
    # Print each detail on a line with nice formatting
    for key, value in details.items():
        print("  + {}: {}".format(key.title(), value))

    # Print a header for the items list
    print("\nItems:\n------")
    # List each item along with a checkbox
    for index, item_name in enumerate(items, start=1):
        print("  {}. {} [ ]".format(index, item_name))

if __name__ == "__main__":
    print_shopping_list(
        "Bread",
        "Milk",
        "Eggs",
        # Pass details as keyword arguments
        store="ShopRite",
        day="Monday",
        time="9:00 P.M."
    )
