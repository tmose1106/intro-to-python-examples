def factorial(n):
    if n == 1 or n == 0:
        return 1
    else:
        return n * factorial(n - 1)

for number in range(20):
    print("{}! = {}".format(number, factorial(number)))
