# We define an arbitrary argument with an asterisk in front of the name
def say_hello_to_people(greeting, *names):
  for name in names:
      print("{}, {}!".format(greeting, name))

# We can call the function with as many arguments as we want
say_hello_to_people("Hello", "Alice", "Bob", "Cármèn")

print() # Print a new line to separate the two calls

# OR we can create a list...
name_list = ["Alice", "Bob", "Cármèn"]

#  ...and put an asterisk in front of the list variable to pass the
# list items as arguments
say_hello_to_people("Hi", *name_list)
