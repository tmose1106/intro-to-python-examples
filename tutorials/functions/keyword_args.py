# We define keyword arguments with two asterisk
def greet_people(**name_and_greeting):
    for name, greeting in name_and_greeting.items():
        print("{}, {}!".format(greeting, name))

# We call the function with names and values separate by an equals sign
greet_people(Alice="Greetings", Bob="Hi", Cármèn="Hoi")

print() # Print a new line to separate the two calls

# We can define out keywords inside of a dictionary...
my_map = {
    "Alice": "Greetings",
    "Bob": "Hi",
    "Cármèn": "Hoi",
}

# ...And use that dictionary as keywords using two asterisk
greet_people(**my_map)
