class SomeClass():

    def __len__(self):
        return 5

def custom_length_function(an_object):
    return an_object.__len__()

if __name__ == "__main__":
    my_object = SomeClass()

    print("Built-in:", len(my_object))
    print("Custom:", custom_length_function(my_object))
