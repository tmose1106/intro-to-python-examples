class ClassName():
    def __init__(self, value):
        """
        This is the function which runs when an object is instantiated. In
        this method, we only store some data in an attribute variable.
        """
        self.attribute = value

    def member_function(self, multiplier):
        """ A function for interacting with the stored data. """
        self.attribute *= multiplier


if __name__ == "__main__":
    my_object = ClassName(3)
    print("Inital value:", my_object.attribute)
    my_object.member_function(3)
    print("New value:", my_object.attribute)
