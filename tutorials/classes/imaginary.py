import math


class ImaginaryNumber():
    """ An imaginary number """

    # Run this method when the class is instantiated
    # Here "self" refers to the class itself
    def __init__(self, real, imaginary):
        #  Add a new attribute to the class using the "string" argument
        # passed when the class is instantiated
        self.real_part = real
        self.imaginary_part = imaginary

    def __str__(self):
        return self.get_rectangular_form()

    def __add__(self, other):
        sum_real = self.real_part + other.real_part
        sum_imaginary = self.imaginary_part + other.imaginary_part
        return ImaginaryNumber(sum_real, sum_imaginary)

    def get_rectangular_form(self):
        return "{}{}{}i".format(str(self.real_part),
                                '+' if self.imaginary_part > 0 else '',
                                str(self.imaginary_part))

    def get_polar_form(self):
        magnitude = math.sqrt((self.real_part ** 2) + (self.imaginary_part ** 2))
        angle = self.imaginary_part / self.real_part
        return u"{}\u2220{}".format(magnitude, angle)


if __name__ == "__main__":

    my_object = ImaginaryNumber(3, 2)
    their_object = ImaginaryNumber(4, -2)
    sum_object = my_object + their_object

    print("{} + {} = {}".format(str(my_object), str(their_object), sum_object.get_polar_form()))
