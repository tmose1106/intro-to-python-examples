class Clock:
    """ A class defining a 24-hour clock. """
    def __init__(self):
        self._hour = 1
        self._minute = 0

    def __str__(self):
        # Zero pad the hour and minute
        return "{:0>2}:{:0>2}".format(self._hour, self._minute)

    @property  # Define this method as a getter for hour
    def hour(self):
        return self._hour

    @hour.setter  # Define this method as a setter for hour
    def hour(self, value):
        if value >= 0 and value < 24:
            self._hour = value
        else:
            raise ValueError("Invalid hour for 24-hour clock")

    @property
    def minute(self):
        return self._minute

    @minute.setter
    def minute(self, value):
        if value >= 0 and value < 60:
            self._minute = value
        else:
            raise ValueError("Invalid minute for 24-hour clock")

if __name__ == "__main__":
    my_clock = Clock()

    print("It is {} right now".format(my_clock))

    my_clock.hour = 13
    my_clock.minute = 52

    print("Now it is {}".format(my_clock))

    my_clock.minute = 60  # This will raise a ValueError
