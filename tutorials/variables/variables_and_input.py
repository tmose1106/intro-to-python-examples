# Prompt the user for input
user_age_string = input("What is your age?: ")

# Convert the input to an integer (fail if it can't be converted)
user_age_integer = int(user_age_string)

# Multiply the integer by 12 to estimate months
user_age_months = user_age_integer * 12

print("You are approximately", user_age_months, "months young!")
