def perfect_squares(n):
    """ Generate n perfect squares. """
    squares = []

    for number in range(n):
        squares.append(number**2)

    return squares


if __name__ == "__main__":
    squares_list = perfect_squares(5)
    print(squares_list)

    for perf_sqr in squares_list:
        print(perf_sqr)
