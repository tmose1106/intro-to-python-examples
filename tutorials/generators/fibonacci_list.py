def fib(n):
    """ Generate n fibonacci numbers. """
    if n < 1:
        return []
    elif n == 1:
        return [0]
    elif n == 2:
        return [0, 1]
    else:
        numbers = [0, 1]

        for index in range(n - 2):
            numbers.append(numbers[index] + numbers[index + 1])

        return numbers


if __name__ == "__main__":
    fib_list = fib(16)

    for fib_number in fib_list:
        print(fib_number, end=' ')

    print()
