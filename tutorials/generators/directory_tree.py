""" Print a text-based directory tree similar to the UNIX tree command. """

import os
import sys

def recursive_iterate_directory(dir_path_string, _depth=0):
    """
    Iterate over directories, yielding files recursively in
    directories.
    """
    # For each item in the directory...
    for dir_item in os.scandir(dir_path_string):
        # Yield the item's name and its depth
        yield (dir_item.name, _depth)
        if dir_item.is_dir():
            # Yield all of the items in sub directories too
            yield from recursive_iterate_directory(dir_item.path, _depth=_depth + 1)


def print_branch(name, depth):
    """ Print a branch of a directory tree. """
    print("{}├── {}".format("│   " * depth, name))


if __name__ == "__main__":
    try:
        path = sys.argv[1] # Read the first argument on the command line

        if not os.path.isdir(path):
            print("Path must be a directory!")
            sys.exit(1)
    except IndexError:
        path = '.' # Set the path to the current directory

    print(path) # Print our starting path at the top of the tree

    for item in recursive_iterate_directory(path):
        print_branch(*item)
