def fib(n):
    """ Generate n fibonacci numbers. """
    if n > 0:
        yield 0

    if n > 1:
        yield 1

    if n > 2:
        n_minus_1 = 1
        n_minus_2 = 0

        for index in range(n - 2):
            new_number = n_minus_1 + n_minus_2
            yield new_number
            n_minus_2 = n_minus_1
            n_minus_1 = new_number


if __name__ == "__main__":
    for fib_number in fib(16):
        print(fib_number, end=' ')

    print()
