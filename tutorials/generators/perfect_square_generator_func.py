def perfect_squares(n):
    """ Generate n perfect squares. """
    for number in range(n):
        yield number**2


if __name__ == "__main__":
    squares_generator = perfect_squares(5)
    print(squares_generator)

    for perf_sqr in squares_generator:
        print(perf_sqr)
