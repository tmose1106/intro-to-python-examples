def get_int_input(prompt):
    """ Force the user to input an integer. """
    # While we do not have a valid user input...
    while True:
        try:
            return int(input(prompt))
        # Except if a value error occurs when the user enters an invalid value
        except ValueError:
            # Scold the user for invalid input
            print("That's no integer, you dumby! Try again!")


if __name__ == "__main__":
    get_int_input("Please enter an integer: ")
