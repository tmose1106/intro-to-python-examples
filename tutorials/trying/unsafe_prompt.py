def get_int_input(prompt):
  # This could fail if the user doesn't pass a valid number
  return int(input(prompt))


if __name__ == "__main__":
    get_int_input("Please enter an integer: ")
