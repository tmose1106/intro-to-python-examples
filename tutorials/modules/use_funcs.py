#  Import our "int_stuff.py" and "user_input.py" scripts so we can use their
# contents
import int_stuff
import user_input

# Get a list of integers from the user
int_list = user_input.get_integer_list()
# Find the maximum integer in the list
max_int = int_stuff.get_maximum_integer(int_list)

print("The greatest integer is {}!".format(max_int))
