""" Functions dealing with gathering user input. """

def get_integer_list():
    """ Get a list of integers from user input. """
    input_string = input("Please enter a list of integers separated by commas:\n")

    input_list = []

    # For each item in the input... (after sepearating)
    for item in input_string.split(','):
        # Convert the item to an integer and append it to the list
        input_list.append(int(item))

    return input_list
