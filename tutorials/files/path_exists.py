""" Check if a file path exists and if so what type it is. """

import os.path

file_path = "sample.txt"

if os.path.exists(file_path):
    print("The path {} exists...".format(file_path))

    if os.path.isfile(file_path):
        print("And it is a file!")
    elif os.path.isdir(file_path):
        print("And it is a directory!")
    elif os.path.islink(file_path):
        print("And it is a shortcut!")
else:
    print("The path {} DOESN\'T exists...".format(file_path))
