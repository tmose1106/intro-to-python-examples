""" Using the Statement Method """

# Open the file
my_file = open("sample.txt")
# Read the contents of the file to a string and print them
print(my_file.read())
# Close the file, because we are done with it
my_file.close()
