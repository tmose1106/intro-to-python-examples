"""
A text editor capable of writing line by line only.
"""

with open("editor.txt", 'w') as editor_file:
    while True:
        input_text = input()

        # If "EOF" is typed, close the editor
        if input_text == "EOF":
            break

        # If text was entered, write it to the file
        if input_text:
            editor_file.write(input_text)

        # Always write a new line
        editor_file.write('\n')
