""" Print a standard relative Python path for any operating system. """

path_parts = ["Documents", "Python"]

# Use the list expansion to pass arbitary arguments
print(os.path.join(*path_parts))
