""" Using the Context Manager Method """

with open("sample.txt") as my_file:
    print(my_file.read())
