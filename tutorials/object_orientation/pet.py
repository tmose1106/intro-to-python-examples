class Pet:
    """ A class defining a general pet. """
    def __init__(self, name, species):
        self._name = name
        self._species = species

    def __str__(self):
        return "{} is a {}".format(self._species, self._name)

    @property  # Add a getter to the object
    def name(self):
        return self._name

    @property
    def species(self):
        return self._species


class Dog(Pet):
    def __init__(self, name, plays_frisbee=False):
        # Super selects the class's base class
        # P.S. I know canine is not a species
        Super(self).__init__(self, name, "Canine")
        self._frisbee = plays_frisbee

    @property
    def frisbee(self):
        return self._frisbee

if __name__ == "__main__":
    print
