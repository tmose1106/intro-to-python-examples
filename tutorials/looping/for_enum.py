item_list = ['a', 'b', 'c', 'd', 'e']

# enumerate() assigns a number to every value in the iterable
# Imagine it like a list of tuples with a number and a value in each tuple
for index, letter in enumerate(item_list):
    print(index, letter)
