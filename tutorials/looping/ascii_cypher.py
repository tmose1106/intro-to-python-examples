# Get the user to input a message
user_input_message = input("Please enter a message: ")
# Create a variable containing an empty string
new_string = ''

# For each character in the user's input...
for a_character in user_input_message:
    # Convert the character to an integer based on the ASCII table
    ascii_int_value = ord(a_character)
    # Get a new character
    new_character = chr(ascii_int_value + 1)
    # Add the new character to the new string
    new_string += new_character

# Print out the new string to the screen
print(new_string)
