countdown_index = 5

while countdown_index:
    # Print the countdown number
    print(countdown_index)
    # Subtract one from countdown_index
    countdown_index -= 1
    # Start over again if not zero

print("All done!")
