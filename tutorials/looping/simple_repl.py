print("Starting simple REPL")

# Start an infinite loop...
while True:
    # Get user input
    user_input = input(">>> ")
    # If user input is "quit"
    if user_input == "quit":
        break # End the loop
    elif user_input == "skip":
        continue # Skip to the next iteration

    print(user_input)

print("Stopping simple REPL")
