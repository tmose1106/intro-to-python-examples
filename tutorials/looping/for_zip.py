english = ["one", "two", "three", "four"]
espanol = ["uno", "dos", "tres", "cuatro"]

# Loop over two lists at once using built-in "zip"
for eng, esp in zip(english, espanol):
    print("English:", eng)
    print("Spanish:", esp)
