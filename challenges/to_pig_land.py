VOWELS = ('a', 'e', 'i', 'o', 'u')


def to_pig(a_string):
    """ Convert words to Pig Latin. """
    out_words = []

    for word in a_string.split(' '):
        first_vowel_index = None

        for char_index, character in enumerate(word):
            if character in VOWELS:
                first_vowel_index = char_index
                break

        if first_vowel_index:
            out_words.append(word[first_vowel_index:] + word[:first_vowel_index] + "ay")
        else:
            out_words.append(word + "way")

    return ' '.join(out_words)


if __name__ == "__main__":
    lines = [
        "the quick brown fox",
        "lorem ipsum dolor sit amet",
        "hello there alice",
        "no way jose",
    ]

    for line in lines:
        print("Before Pig Latin:", line)
        print("After Pig Latin:", to_pig(line))
