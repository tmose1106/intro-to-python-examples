MAP = """+------------------------+
¦ 34 ¦ 21 ¦ 32 ¦ 41 ¦ 25 ¦
+----+----+----+----+----¦
¦ 14 ¦ 42 ¦ 43 ¦ 14 ¦ 31 ¦
+----+----+----+----+----¦
¦ 54 ¦ 45 ¦ 52 ¦ 42 ¦ 23 ¦
+----+----+----+----+----¦
¦ 33 ¦ 15 ¦ 51 ¦ 31 ¦ 35 ¦
+----+----+----+----+----¦
¦ 21 ¦ 52 ¦ 33 ¦ 13 ¦ 23 ¦
+------------------------+"""

coordinates = [] # A list to contain the coordinates

for index, a_line in enumerate(MAP.split('\n')):
    # Skip the line if it isn't odd (meaning it is just the table formatting)
    if not index % 2:
        continue

    # Cut both ends and get all of the coordinates in the line by splitting
    coordinates_in_line = a_line[2:-2].split(" ¦ ")
    # Add the coordinates to the main list
    coordinates.extend(coordinates_in_line)

current_index = 0
search_coordinate = "11"

while True:
    print("Searching coordinate {}...".format(search_coordinate))
    # Note that I use two extra pairs parenthesis here to be more explicit,
    # but they could be taken out and still reach the same result
    search_index = ((int(search_coordinate[0]) - 1) * 5) + (int(search_coordinate[1]) - 1)
    new_coordinate = coordinates[search_index]

    if new_coordinate == search_coordinate:
        print("Treasure found at {}!".format(search_coordinate))
        break
    else:
        print("No treasure at {} :-(".format(search_coordinate))
        search_coordinate = new_coordinate
