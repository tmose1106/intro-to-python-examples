UNITS = {
    "Twenty": 2000,
    "Ten": 1000,
    "Five": 500,
    "One": 100,
    "Quarter": 25,
    "Dime": 10,
    "Nickle": 5,
    "Penny": 1,
}


def get_change(value):
    # Convert the value to an integer in units of pennies
    remaining = int(value * 100)
    change = {}

    for unit_name, unit_value in UNITS.items():
        if unit_value <= remaining:
            # Get a rounded integer of the number of each unit needed
            unit_count = int(remaining // unit_value)
            # Save this number to a dictionary with the name of the unit
            change[unit_name] = unit_count

            remaining -= unit_count * unit_value

    return change


if __name__ == "__main__":
    import pprint

    for change_amount in [0.23, 6.27, 16.05, 23.85]:
        print("Making change for:", change_amount)

        for name, value in get_change(change_amount).items():
            print(name, value)
