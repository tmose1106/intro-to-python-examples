temp_reading = input("Please enter a temperature reading: ")

# Read everything before the first character as a float
temp_value = float(temp_reading[:-1])
# Read the last character as the unit
temp_unit = temp_reading[-1]

if temp_unit in ('C', 'F', 'K'):
    if temp_unit == 'C':
        celcius = temp_value
        fahrenheit = celcius * (9 / 5) + 32
        kelvin = celcius + 273.15
    elif temp_unit == 'F':
        fahrenheit = temp_value
        celcius = (fahrenheit - 32) / (5 / 9)
        kelvin = (fahrenheit + 459.67) * (5 / 9)
    elif temp_unit == 'K':
        kelvin = temp_value
        celcius = kelvin - 273.15
        fahrenheit = kelvin * (9 / 5) - 459.67

    print("Celcius:", round(celcius, 2))
    print("Fahrenheit:", round(fahrenheit, 2))
    print("Kelvin:", round(kelvin, 2))

else:
    print("Invalid unit provided:", temp_unit)
