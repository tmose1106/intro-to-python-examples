************************
Intro to Python Examples
************************

This repository contains code examples from the `Intro to Python`_ tutorials.

Getting the Code
================

The best way to get access to this code is to use Git directly. If you do not
know how to use Git, do not fear! This repository can be easily downloaded
using the GitLab website. Simply click the download button with the cloud, and
click *"Download zip"*.

.. image:: ./download_example.png
  :alt: Download Example

Licensing
=========

This project is released under the terms of the `MIT/Expat`_ Software License.
For more information read the included ``LICENSE.txt`` file.

.. _`MIT/Expat`: https://spdx.org/licenses/MIT.html
