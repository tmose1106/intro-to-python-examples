"""
Create a reStructuredText Table (Simple) from a .csv file

Usage: csv2rst.py FILENAME
"""

# Get csv to work with comma separated value files
import csv
# Get sys to work with arguments and return codes
import sys

try:
    # Attempt to get a file path from the command line
    file_path = sys.argv[1]
except IndexError:
    sys.exit("No file path argument provided!")

try:
    with open(file_path) as file_object:
        # Get a list of csv rows
        rows = list(csv.reader(file_object))
except FileNotFoundError:
    sys.exit(f"File '{file_path}' does not exist!")

row_dividers = []
column_formatters = []

# For each column in the file...
for column in zip(*rows):
    # Set the width to the width of the longest item in the column
    column_width = max((len(item) for item in column))
    # Add a divider the width of the column
    row_divders.append('=' * column_width)
    # Add a format string left padding items to the width of the column
    column_formatters.append(f"{{:<{column_width}}}")

divider_string = ' '.join(row_dividers)
column_format_string = ' '.join(column_formatters)

print(divider)
# Print the first row item which we assume is composed of column titles
print(column_format_string.format(*rows[0]))
print(divider)

for row in rows[1:]:
    print(column_format_string.format(*row))

print(divider)
