========= ============== ==========================================================
Name      Default Value  Description                                               
========= ============== ==========================================================
extension \".mustache\"  Extension used for template files                         
partials  \"_partials\"  Directory containing partial files relative to the source 
templates \"_templates\" Directory containing template files relative to the source
========= ============== ==========================================================
