"""
A quick Python 3 script for scraping metadata from a Bandcamp page
as it is formatted on August 18, 2018.

Usage: scraper.py PAGE_URL
"""

# Get the datetime module for handling date strings
import datetime
# Get the sys module for command line arguments and exit return codes
import sys

# Get the BeautifulSoup4 module for parsing web pages
import bs4
# Get the requests module for getting web pages from the internet
import requests

try:
    # Get the request page's URL as an argument
    page_url = sys.argv[1]
except IndexError:
    print("No URL provided!")
    # End the program here with return code 1
    sys.exit(1)

# Get the web page using the provided URL
request_obj = requests.get(page_url)

# Allow Beautiful Soup to parse the web page's content using html5lib
html_obj = bs4.BeautifulSoup(request_obj.text, "html5lib")

# Select a div with the id of "name-section"
title_div = html_obj.find("div", id="name-section")

# Select the h2 inside of the title div with the class of "trackTitle"
release_name = title_div.find("h2", class_="trackTitle")

print("Title:", release_name.contents[0].strip())

# Select the link inside of the h3 inside of the title div
# Note: this may just be the label name here and not the artist
release_artist = title_div.find("h3").find("a")

print("Artist:", release_artist.contents[0].strip())

# Select a div with the classes "tralbumData" and "tralbum-credits"
release_date = html_obj.find("div", class_="tralbumData tralbum-credits")

# The release date is always formatted as "released {month} {day}, {year}"
# Get a string of just the date by cutting off "released "
date_string = release_date.contents[0].strip()[9:]

# Parse date string into a datetime object using strftime/strptime formatting
# Note: "strptime" (string parse time)
date_object = datetime.datetime.strptime(date_string, "%B %d, %Y")

# Note: "strftime" (string format time)
print("Date:", date_object.strftime("%Y-%m-%d"))

# Get the "table" tag with the id "track_table"
# This contains each track within the release as a "tr" tag
track_table = html_obj.find("table", id="track_table")
tracks = track_table.find_all("tr", itemprop="tracks")

print() # Print a blank line for style

# For each track in the table...
for track_number, track_tr in enumerate(tracks, start=1):
    track_cell = track_tr.find("td", class_="title-col")
    # Get the track title from the "span" tag with "itemprop" of "name"
    title = track_cell.find("span", itemprop="name")
    # Get the track length from the "span" tag with "class" of "time"
    length = track_cell.find("span", class_="time")
    print(str(track_number).zfill(2), title.contents[0], length.contents[0].strip())
