from collections import Counter, namedtuple
from random import choice, shuffle

Card = namedtuple("Card", ["suit", "value"])


SUITS = ("Club", "Diamond", "Heart", "Spade")

VALUES = ("Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King")


class Player:
    def __init__(self, name, deck, players):
        self._deck = deck  # A reference to the deck
        self._players = players  # A reference to the player list

        self.name = name
        self.hand = {deck.pop() for _ in range(7)}  # The players set of cards
        self.books = set()  # The players complete sets of cards

    def _request_value(self, others):
        pass

    def play_turn(self):
        print(f"It is {self.name}'s turn!")

        while True:
            if not self.hand:
                break

            player, value = self._request_value([player for player in self._players if player is not self])

            requested = {card for card in player.hand if card.value == value}

            print(f"{self.name} asked {player.name} for {value}...")

            if requested:
                self.hand.update(requested)

                player.hand -= requested  # Remove requested cards

                print(f"{self.name} got cards from {player.name}")
            else:
                print(f"{player.name} said GO FISH! {self.name} draws card...")

                if not self._deck:
                    print("No cards left in deck...")
                    break

                drawn = self._deck.pop()  # GO FISH

                self.hand.add(drawn)

                # Go again if requested value was drawn
                if drawn.value == value:
                    print("Got requested value, go again!")
                else:
                    break

            # Get cards of requested value
            value_book = {card for card in self.hand if card.value == value}

            if len(value_book) == 4:
                print(f"{self.name} got book of {value}")

                self.hand -= value_book  # Remove book from hand

                self.books.add(value)

        print("End turn")


class HumanPlayer(Player):
    def _request_value(self, others):
        print("What player would you like to ask?")

        for index, player in enumerate(others):
            print(f"{index}: {player.name}")

        player, value = None, None

        while player is None:
            try:
                player = others[int(input("Pick a player: "))]
            except (KeyError, ValueError):
                print("Invalid choice, pick again please")

        card_counter = Counter([card.value for card in self.hand])

        print("What value would you like to ask for?")

        for index, item in enumerate(card_counter.items()):
            print(f"{index}: {item[0]} ({item[1]})")

        while value is None:
            try:
                value = tuple(card_counter.values())[int(input("Pick a value: "))]
            except (KeyError, ValueError):
                print("Invalid choice, pick again please")

        return player, value


class ComputerPlayer(Player):
    def _request_value(self, others):
        return choice(others), choice(tuple({card.value for card in self.hand}))


if __name__ == "__main__":
    deck = [Card(suit, value) for value in VALUES for suit in SUITS]

    shuffle(deck)

    players = []

    for index in range(4):
        if index == 0:
            name, player_type = input("What is your named?: "), HumanPlayer
        else:
            name, player_type = chr(64 + index), ComputerPlayer

        # Comment above and uncomment below to have only computer players
        # name, player_type = chr(65 + index), ComputerPlayer

        players.append(player_type(name, deck, players))

    game_over = False

    while not game_over:
        for player in players:
            if not deck:
                game_over = True
                break

            player.play_turn()

            if not player.hand:
                game_over = True
                break

    scores = [len(player.books) for player in players]

    highest_score = max(scores)

    winners = [player.name for player, score in zip(players, scores) if score == highest_score]

    if len(winners) == 1:
        print(f"Player {winners[0]} wins!")
    else:
        print(f"Tie between {winners}!")
